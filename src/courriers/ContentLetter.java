package courriers;

public abstract class ContentLetter extends Letter {

	protected Letter l;
	/**
	 * create the content of the letter who content : a send , a receiver and a letter
	 * @param l
	 */
	public ContentLetter(Letter l) {
		this.sender = l.getSender();
		this.receiver = l.getReceiver();
		this.l = l;
	}


	public String getContent() { return l.getContent(); }	
}
