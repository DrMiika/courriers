package courriers;

import habitant.Inhabitants;

public class SimpleLetter extends TypeLetter {
	

	/* Méthodes */
	/**
	 * Constructor of SimpleLetter
	 * @param sender
	 * @param receiver
	 * @param contenu
	 */
	public SimpleLetter(Inhabitants sender, Inhabitants receiver, Contenu contenu) {
		super(sender, receiver, contenu);
		this.contenu = contenu;
		this.price = 1.0;
	}
	
	/**
	 * Return the description of this letter
	 * @return a String who content the description of this letter
	 */
	public String getDescription() { return "a simple letter"; }	

	/**
	 * Execute some action for this letter like debiter and crediter
	 */
	public void doAction() {
		this.getSender().getAcc().debiter(this.coutLetter());
		System.out.println("- "+ this.coutLetter() +" euros are debited from "+ this.getSender().getName() +" account whose balance is now "+ this.getSender().getAcc().getAmount() +" euros");
	}

}
