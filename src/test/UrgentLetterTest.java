package test;

import static org.junit.Assert.*;
import habitant.BankAccount;
import habitant.Inhabitants;

import org.junit.Before;
import org.junit.Test;

import courriers.City;
import courriers.Contenu;
import courriers.ContenuLettres;
import courriers.PromisoryLetter;
import courriers.RegisteredLetter;
import courriers.SimpleLetter;
import courriers.UrgentLetter;

public class UrgentLetterTest {
	protected Contenu content1;
	protected Contenu content2;
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected SimpleLetter sLetter;
	protected PromisoryLetter pLetter;
	protected UrgentLetter uLetter1;
	protected UrgentLetter uLetter2;
	protected City city;
	@Before
	public void init() {
		city=new City();
		this.content1 = new ContenuLettres("En route, Watson!");
		this.content2 = new ContenuLettres(100.0);
		sender = new Inhabitants("James", city, new BankAccount(10000.0));
		receiver = new Inhabitants("Watson", city, new BankAccount(10000.0));
		sLetter =new SimpleLetter(sender ,receiver ,content1);
		pLetter = new PromisoryLetter(sender, receiver, content2);
		uLetter1 = new UrgentLetter(sLetter);
		uLetter2 = new UrgentLetter(pLetter);
	}
	
	@Test
	public void testUrgentLetter() {
		uLetter1.doAction();
		assertEquals(9998.0, uLetter1.getSender().getAcc().getAmount(), 0.0);
		assertEquals("a Urgent Letter whose content a simple letter", uLetter1.getDescription());
		System.out.println(uLetter1.getDescription());
	}



}
