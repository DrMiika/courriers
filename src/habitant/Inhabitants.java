package habitant;

import courriers.City;

public class Inhabitants extends City {

	/* Attributs */
	private String name;
	private City city;
	private BankAccount acc;
	
	/* Methodes */
	/**
	 * Constructor of Inhabitants
	 * @param name
	 * @param city
	 * @param acc
	 */
	public Inhabitants(String name, City city, BankAccount acc ) {
		this.name = name;
		this.city = city; 
		this.setAcc(acc);
		
	}
	
	/**
	 * Return the BankAccount of this Inhabitants
	 * @return a BankAccount
	 */
	public BankAccount getAcc() {
		return this.acc;
	}
	
	/**
	 * Set a BankAccount for this Inhabitants
	 * @param acc The account to set for this Inhabitants
	 */
	public void setAcc(BankAccount acc) {
		this.acc = acc;
	}
	
	/**
	 * Return the name of this Inhabitants
	 * @return A String who contents the name of this Inhabitants
	 */
	public String getName() {
		return this.name;	
	}
	
	/**
	 * Return the city of this Inhabitants
	 * @return A city 
	 */
	public City getCity() {
		return this.city;
	}

}
