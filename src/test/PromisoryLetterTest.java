package test;

import courriers.*;
import habitant.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PromisoryLetterTest {
	protected ContenuLettres contenu;
	protected ContenuLettres contenu2;
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected PromisoryLetter pLetter ;
	protected City city;
	
	@Before
	public void init() {
		city=new City();
		sender = new Inhabitants("James", city, new BankAccount(10000.0));
		receiver = new Inhabitants("Watson", city, new BankAccount(10000.0));

	}
	@Test
	public void testCoutLetter() {
		this.contenu = new ContenuLettres(100);
		pLetter =new PromisoryLetter(sender ,receiver ,contenu);
		assertEquals(2.0, pLetter.coutLetter(), 0.0);
	}

	@Test
	public void testPromisoryLetter() {
		this.contenu = new ContenuLettres(100.0);
		pLetter =new PromisoryLetter(sender ,receiver ,contenu);
		pLetter.doAction();
		assertEquals(9898.0, pLetter.getSender().getAcc().getAmount(), 0.0);
		assertEquals("a promissory letter", pLetter.getDescription());

		
		
	}

}
