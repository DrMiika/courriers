
package courriers;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class PostBox {

	/* Attributs */
	private ArrayList<Letter> listLetter;
	
	/* Constructor */
	public PostBox() {
		listLetter = new ArrayList<Letter>();
	}
	
	/* Méthodes */
	
	/**
	 * add a letter to postbox
	 * @param l of type letter
	 */
	public void addLetter(Letter l) {
		listLetter.add(l);
	}
	
	/**
	 * remove a letter of the postbox
	 * @return Letter 
	 */
	public Letter removeLetter() throws NoSuchElementException {
		if (!listLetter.isEmpty()) { 
			return listLetter.remove(0);
		}
		else { throw new NoSuchElementException();}
	}
	
	/**
	 * get the list of the postbox
	 * @return ArrayList<Letter> 
	 */
	public ArrayList<Letter> getList() {
		return listLetter;
	}
	
}
