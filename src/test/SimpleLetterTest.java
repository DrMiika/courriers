package test;

import courriers.*;
import habitant.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleLetterTest {
	protected ContenuLettres contenu;
	protected ContenuLettres contenu2;
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected SimpleLetter sLetter ;

	@Before
	public void init() {
		this.contenu = new ContenuLettres("test de contenu");
		sLetter =new SimpleLetter(sender ,receiver ,contenu);
	}
	
	@Test
	public void testCoutLetter() {
		assertEquals(1.0, sLetter.coutLetter(), 0.0);
	}

	@Test
	public void testSimpleLetter() {
		assertEquals("a text content (test de contenu)",sLetter.getContent());
	}
	
	@Test
	public void testgetDescription() {
		assertEquals("a simple letter", sLetter.getDescription());
		
	}

}
