package courriers;

import habitant.Inhabitants;

public class AckLetter extends SimpleLetter {
	
	/**
	 * constructor for the creation of the acknowledge letter
	 * @param sender
	 * @param receiver
	 * @param contenu
	 */
	public AckLetter(Inhabitants sender, Inhabitants receiver,Contenu contenu) {
		super(receiver, sender, contenu);
	}
}
