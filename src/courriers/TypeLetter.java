package courriers;

import habitant.Inhabitants;

public abstract class TypeLetter extends Letter {
	
	/**
	 * Constructor for different Type of Letter
	 * @param sender
	 * @param receiver
	 * @param contenu
	 */
	public TypeLetter(Inhabitants sender, Inhabitants receiver, Contenu contenu) {
		this.sender = sender;
		this.receiver = receiver;
		this.contenu = contenu ;
	}
	
	/**
	 * Return the content of this letter
	 * @return a String which content the content of this letter
	 */
	public String getContent() { return this.contenu.contentLetter(); }	
}
