package courriers;

import habitant.Inhabitants;

public class ThanksLetter extends SimpleLetter {
	
	protected Contenu moneyContent;

	/**
	 * Constructor of ThanksLetter
	 * @param sender
	 * @param receiver
	 * @param contenu
	 */
	public ThanksLetter(Inhabitants sender, Inhabitants receiver,Contenu contenu) {
		super(sender, receiver, new ContenuLettres("Thank for a promissory note letter whose content is a money content ("+contenu.getMoneyContent()+")"));
		this.moneyContent=contenu;
	}
	
	/**
	 * Execute some action for this letter like debiter and crediter
	 */
	public void doAction() {
		this.getSender().getAcc().crediter(moneyContent.getMoneyContent()-this.coutLetter());
		System.out.println("+ "+ this.getSender().getName() +" account is credited with "+ moneyContent.getMoneyContent() + " euros; its balance is now "+ this.getSender().getAcc().getAmount() +" euros");
		this.flagDoAction = false;
	}
}
