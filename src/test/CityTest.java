package test;

import static org.junit.Assert.*;
import habitant.BankAccount;
import habitant.Inhabitants;

import org.junit.Before;
import org.junit.Test;

import courriers.City;
import courriers.ContenuLettres;
import courriers.RegisteredLetter;
import courriers.SimpleLetter;

public class CityTest {
	protected ContenuLettres contenu;
	protected ContenuLettres contenu2;
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected SimpleLetter sLetter;
	protected SimpleLetter sLetter2;
	protected RegisteredLetter rLetter;
	protected City city;

	@Before
	public void init() {
		city=new City();
		this.contenu = new ContenuLettres("En route, Watson!");
		sender = new Inhabitants("James", city, new BankAccount(10000.0));
		receiver = new Inhabitants("Watson", city, new BankAccount(10000.0));
		sLetter =new SimpleLetter(sender ,receiver ,contenu);
		sLetter2 =new SimpleLetter(sender ,receiver ,contenu);
		rLetter = new RegisteredLetter(sLetter);
	}

	@Test
	public void testSendLetter() {
		city.addToWaitingList(sLetter);
		city.sendLetter();
		assertFalse(city.getPostBox().getList().isEmpty());
	}


	@Test
	public void testDistributeLetters() {
		city.addToWaitingList(rLetter);
		city.addToWaitingList(sLetter2);
		city.sendLetter();
		city.distributeLetters();
		assertTrue(city.getPostBox().getList().isEmpty());
		city.sendLetter();
		assertFalse(city.getPostBox().getList().isEmpty());
	}
	@Test
	public void testBank(){
		sender = new Inhabitants("James", city, new BankAccount(0.0));
		sender.getAcc().crediter(100.0);
		assertEquals(100.0,sender.getAcc().getAmount(),0.0);
		
	}

}
