package courriers;

import habitant.BankAccount;
import habitant.Inhabitants;

import java.util.ArrayList;
import java.util.Random;

import courriers.PostBox;

public class City {

	/* Attibuts */
	protected PostBox postBox;
	protected ArrayList<Letter> waitingList;
	
	/* Constructor */
	public City() {
		postBox = new PostBox();
		waitingList = new ArrayList<Letter>();
	}

	/* Méthodes */
	/**
	 * add the letter to the wait list  
	 * @param l letter 
	 */
	public void addToWaitingList(Letter l) {
		waitingList.add(l);
	}
	
	/**
	 * put a letter to the box of city 
	 */
	public void sendLetter() {
		while(!waitingList.isEmpty()) { postBox.addLetter(waitingList.remove(0)); }
	}
	
	/**
	 * distribute the letter of the city content on the postbox
	 */
	public void distributeLetters() {
		while ( !postBox.getList().isEmpty() ) {
			Letter l = postBox.removeLetter();
			System.out.println(l.getSender().getName()+ " mails " + l.getDescription() + " whose is " + l.getContent() +" to " + l.getReceiver().getName() + " for a cost of " + l.coutLetter() + " euros." );
			l.doAction();
		}
	}
	/**
	 * return the Postbox of the city 
	 * @return Postbox
	 */
		public PostBox getPostBox(){
		return postBox;
		
	}
	
	public static void main(String[] args) {
		City city = new City();
		Inhabitants inhabitants[] = new Inhabitants[100];
		for (int i=0; i<100; i++) {
			inhabitants[i] = new Inhabitants("inhabitants-" + (i+1), city, new BankAccount(500.0));
		}
		Random rand = new Random();
		
		//Envoi de Lettres
		for (int k=1; k<7; k++){
			// Nombre d'habitants qui vont envoyer une lettre
			int nbHabitantRand= rand.nextInt(10);
			System.out.println(nbHabitantRand);
			System.out.println("\n Jour " + k+ "\n");
			for (int i=1; i<=nbHabitantRand; i++) {
				//Habitants qui envoient une lettre
				Inhabitants sender;
				Inhabitants receiver;
				int nbSender = rand.nextInt(100);
				sender = inhabitants[nbSender];
				int nbReceiver = rand.nextInt(100);
				while (nbReceiver == nbSender) nbReceiver = rand.nextInt(100);
				receiver = inhabitants[nbReceiver];
			
				//Lettres envoyées
				int nbLetter = rand.nextInt(2);
				Letter l=null;
				Contenu content;
				int j = 0;
				switch(nbLetter) {
				case 0:
					content = new ContenuLettres("Bla bla");
					l = new SimpleLetter(sender, receiver, content);
					break;
				case 1:
					content = new ContenuLettres(5.0);
					l = new PromisoryLetter(sender, receiver, content);
					break;
				default: 
					break;
				}
				nbLetter=rand.nextInt(3);
				while(j != nbLetter ){
					int nbLetter2 = rand.nextInt(2);
					switch(nbLetter2) {
					case 0:
						l = new RegisteredLetter(l);
						break;
					case 1:
						l = new UrgentLetter(l);
						break;
					default: 
						break;
					}
					j++;
				}
				city.addToWaitingList(l);
			}
			city.sendLetter();
			city.distributeLetters();
		}
		
	}
}
