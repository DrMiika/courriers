package courriers;

import courriers.Contenu;
import habitant.Inhabitants;

public abstract class Letter {
	
	/* Attributs */
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected Contenu contenu;
	protected boolean flagDoAction = false;
	protected double price;
	
	/* Méthodes */
	
	/**
	 * return the inhabitants who send the letter
	 * @return Inhabitants
	 */
	public Inhabitants getSender(){ return this.sender; }
	/**
	 * return the inhabitants who receiver the letter
	 * @return Inhabitants
	 */
	public Inhabitants getReceiver(){ return this.receiver; }
	
	public void setPrice(double f) { this.price = f; }
	/**
	 * return the content of the letter
	 * @return String
	 */
	public abstract String getContent();
	/**
	 * give th price of the letter
	 * @return price
	 */
	public double coutLetter() { return this.price; }
	/**
	 * return the description of the letter , is type
	 * @return String
	 */
	public abstract String getDescription();
	/**
	 * realize the action of the letter 
	 * 
	 */
	public abstract void doAction();
}
