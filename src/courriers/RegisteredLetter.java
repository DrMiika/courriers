package courriers;

public class RegisteredLetter extends ContentLetter {
	
	/**
	 * Constructor of RegisteredLetter
	 * @param l
	 */
	public RegisteredLetter(Letter l){
		super(l);
		this.price = 15.0 +	l.coutLetter();
	}

	/**
	 * Return the description of this letter
	 * @return a String who content the description of this letter
	 */
	public String getDescription() { return "a Registered Letter whose content "+ this.l.getDescription(); }

	/**
	 * Execute some action for this letter like debiter and crediter
	 */
	public void doAction(){
		this.l.setPrice(this.coutLetter());
		Letter ack = this.createLetter();
		ack.getSender().getCity().addToWaitingList(ack);
		this.l.doAction();
		
	}
	
	/**
	 * Create an aknowledgement of receipt for this letter
	 * @return a AckLetter
	 */
	public Letter createLetter() {
		return new AckLetter(
				this.getReceiver(), 
				this.getSender(),
				new ContenuLettres("Aknowledgement of receipt whose content "+ this.l.getDescription() ));
	}

}
