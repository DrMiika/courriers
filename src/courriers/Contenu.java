package courriers;

public interface Contenu {
	/**
	 * return the content of the letter in String 
	 * @return Content
	 */
	public String contentLetter();
	
	/**
	 * return the amount of the content of the letter
	 * @return amount
	 */
	public double getMoneyContent();
}
