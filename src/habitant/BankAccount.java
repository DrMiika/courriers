package habitant;


public class BankAccount {

	private double amount;
	
	/**
	 * Constructor of BankAccount
	 * @param money
	 */
	public BankAccount(double money) {
		this.amount=money;;
	}

	/**
	 * Used to add money on this account
	 * @param acre the money to add on this account
	 */
	public void crediter(double acre) {
		this.amount += acre;
	}
	
	/**
	 * Used to remove money from this account
	 * @param adeb the money to remove from this account
	 */
	public void debiter(double adeb){
		if (this.amount >= adeb)
			this.amount -= adeb;
	}
	
	/**
	 * Return the current money on this account
	 * @return a Double who content the money on this account
	 */
	public double getAmount() {
		return this.amount;
	}
}
