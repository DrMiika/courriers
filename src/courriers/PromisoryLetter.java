package courriers;

import habitant.Inhabitants;

public class PromisoryLetter extends TypeLetter {

	/* Constructor */
	/**
	 * Constructor
	 * @param sender
	 * @param receiver
	 * @param contenu
	 */
	public PromisoryLetter(Inhabitants sender, Inhabitants receiver, Contenu contenu) {
		super(sender, receiver ,contenu);
		this.contenu = contenu;
		this.price = 1.0 + contenu.getMoneyContent()/100.0;
	}

	/* Méthodes */
	/**
	 * Return the description of this letter
	 * @return a String who content the description of this letter
	 */
	public String getDescription(){ return "a promissory letter"; }
	
	/**
	 * Execute some action for this letter like debiter and crediter
	 */
	public void doAction(){
		this.getSender().getAcc().debiter(this.coutLetter()+contenu.getMoneyContent());
		System.out.println("- "+ (this.coutLetter()+contenu.getMoneyContent()) +" euros are debited from "+ this.getSender().getName() +" account whose balance is now "+ this.getSender().getAcc().getAmount() +" euros");
		Letter thx = this.createLetter();
		thx.getSender().getCity().addToWaitingList(thx);
	}

	/**
	 * Create a ThanksLetter for this letter
	 * @return a ThanksLetter
	 */
	public Letter createLetter() {
		return new ThanksLetter(this.getReceiver(), this.getSender(), contenu);
	}
}
