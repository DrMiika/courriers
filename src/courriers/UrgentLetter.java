package courriers;

public class UrgentLetter extends ContentLetter{

	/**
	 * Constructor of UrgentLetter
	 * @param l
	 */
	public UrgentLetter(Letter l) {
		super(l);
		this.price = l.coutLetter()*2;
	}

	/**
	 * Return the description of this letter
	 * @return a String who content the description of this letter
	 */
	public String getDescription() { return "a Urgent Letter whose content " + this.l.getDescription(); }

	/**
	 * Execute some action for this letter like debiter and crediter
	 */
	public void doAction(){
		this.l.setPrice(this.coutLetter());
		this.l.doAction();
		
	}
}
