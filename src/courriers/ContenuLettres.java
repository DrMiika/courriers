package courriers;

public class ContenuLettres implements Contenu{
		
	private double moneyContent = 0.0;
	private String message;
	/**
	 *create a content with a message string
	 * 
	 */
	public ContenuLettres(String message){
		this.message=message;
	}
	/**
	 *create a content with amount of money
	 * 
	 */
	public ContenuLettres(double moneyContent){
		this.moneyContent=moneyContent;
	}
	
	/**
	 * return the content of the letter in String 
	 * @return Content
	 */
	public String contentLetter() {
		if (moneyContent != 0.0 ) {
			return "a money content (" + this.moneyContent + ")";
		}
		return "a text content (" + this.message + ")";
	}
	/**
	 * return the amount of the content of the letter
	 * @return amount
	 */
	public double getMoneyContent(){
		return moneyContent;
	}

}

