package test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;

import courriers.*;
import org.junit.Test;
import habitant.Inhabitants;


public class PostBoxTest {
	 protected PostBox pb;
	 protected Letter sLetter;
	 protected Inhabitants sender;
	 protected Inhabitants receiver;
	 protected Contenu content;
	 
	 @Before
	 public void init() {
		 pb = new PostBox();
		 content = new ContenuLettres("coucou");
		 sLetter = new SimpleLetter(sender, receiver, content);
	 }
	 
	 @Test
	 public void TestPostBoxCreation() {
		 pb = new PostBox();
		 assertNotNull(pb);
	 }
	 
	 @Test
	 public void TestAddLetter() {
		 pb.addLetter(sLetter);
		 assertFalse(pb.getList().isEmpty());
	 }

	 @Test
	 public void TestRemoveLetter() {
		 pb.addLetter(sLetter);
		 Letter l = pb.removeLetter();
		 assertEquals(sLetter, l);
	 }
	 
	 @Test
	 public void TestRemoveLetterException() {
		 try { pb.removeLetter(); }
		 catch (NoSuchElementException e) { return; }
		 fail("Should throw the exception");
	 }
}