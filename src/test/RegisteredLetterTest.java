package test;
import courriers.*;
import habitant.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RegisteredLetterTest {
	protected Contenu content1;
	protected Contenu content2;
	protected Inhabitants sender;
	protected Inhabitants receiver;
	protected SimpleLetter sLetter;
	protected PromisoryLetter pLetter;
	protected RegisteredLetter rLetter1;
	protected RegisteredLetter rLetter2;
	protected City city;
	
	@Before
	public void init() {
		city=new City();
		this.content1 = new ContenuLettres("En route, Watson!");
		this.content2 = new ContenuLettres(100.0);
		sender = new Inhabitants("James", city, new BankAccount(10000.0));
		receiver = new Inhabitants("Watson", city, new BankAccount(10000.0));
		sLetter =new SimpleLetter(sender ,receiver ,content1);
		pLetter = new PromisoryLetter(sender, receiver, content2);
		rLetter1 = new RegisteredLetter(sLetter);
		rLetter2 = new RegisteredLetter(pLetter);
	}
	


	@Test
	public void testDoAction() {
		rLetter1.doAction();
		assertEquals(9984.0, rLetter1.getSender().getAcc().getAmount(), 0.0);
	}

}
